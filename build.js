'use strict';

const jsonminify = require("jsonminify");
const UglifyJS = require('uglify-es');
const webExt = require('web-ext').default;
const path = require('path');
const fs = require('fs-extra');

module.exports = async function() {
  var sourcePath = path.join(__dirname, 'src');
  var buildPath = path.join(__dirname, 'build');
  var artifactsPath = path.join(buildPath, 'artifacts');
  var minifiedPath = path.join(buildPath, 'minified');
  try {
    await fs.ensureDir(buildPath);
    await fs.ensureDir(artifactsPath);
    await fs.ensureDir(minifiedPath);
    await fs.emptyDir(minifiedPath);
  } catch (err) {
    throw {
      message: 'Could not create or access the build directory.',
      fsError: err
    };
  }
  var files = [];
  var directories = [];
  var filesInDir = async (dir, base) => {
    if (base == undefined) {
      var base = '';
    }
    var dirlisting = await fs.readdir(dir);
    for (var file of dirlisting) {
      var fullPath = path.join(dir, file);
      if ((await fs.lstat(fullPath)).isDirectory()) {
        await filesInDir(fullPath, path.join(base, file));
        directories.push(path.join(base, file));
      } else {
        files.push(path.join(base, file));
      }
    }
  }
  try {
    await filesInDir(sourcePath);
    for (var directory of directories) {
      await fs.ensureDir(path.join(minifiedPath, directory));
    }
    var filePromises = [];
    var doFile = async (file) => {
      var inputPath = path.join(sourcePath, file);
      var outputPath = path.join(minifiedPath, file);
      if (file.toLowerCase().endsWith('.js')) {
        var minifierInput = {};
        minifierInput[file] = await fs.readFile(inputPath, 'utf-8');
        var minified = UglifyJS.minify(minifierInput, {
          mangle: false
        });
        if (minified.error) {
          throw minified.error;
        }
        await fs.writeFile(
          outputPath,
          minified.code,
          'utf-8'
        );
      } else if (file.toLowerCase().endsWith('.json')) {
        await fs.writeFile(
          outputPath,
          jsonminify(await fs.readFile(inputPath, 'utf-8')),
          'utf-8'
        );
      } else {
        await fs.copyFile(inputPath, outputPath);
      }
    }
    for (var file of files) {
      filePromises.push(doFile(file));
    }
    await Promise.all(filePromises);
    // separately copy the license
    fs.copy(path.join(__dirname, 'LICENSE'), path.join(minifiedPath, 'LICENSE'));
  } catch (err) {
    throw {
      message: 'An error occurred while minifying files.',
      fsError: err
    };
  }
  try {
    var result = await webExt.cmd.build({
      sourceDir: minifiedPath,
      artifactsDir: artifactsPath
    }, {
      shouldExitProgram: false,
    });
  } catch (err) {
    throw {
      message: 'Could not build the extension.',
      webExtError: err
    };
  }
  return result;
}

if (!module.parent) {
  module.exports().then((result) => {
    console.log('Built successfully.');
    console.log(result);
  }).catch((err) => {
    console.error(err);
    return process.exit(2);
  });
}