'use strict';

var isChrome = false;

if (typeof browser == 'undefined' && typeof chrome != 'undefined') {
  isChrome = true;
  var browser = chrome;
}

function p(thisArg, func) {
  // this function turns chrome's callback-based system into a promise, similar to how firefox works.
  var functionArguments = Array.prototype.slice.call(arguments, 2); // remove the first two arguments, which are thisArg and the function to be called
  if (isChrome) {
    return new Promise((resolve, reject) => {
      // add chrome callback as the last argument
      functionArguments.push(function() {
        if (browser.runtime.lastError) {
          reject.apply(thisArg, arguments);
        } else {
          resolve.apply(thisArg, arguments);
        }
      });
      // run the function
      try {
        func.apply(thisArg, functionArguments);
      } catch (ex) {
        reject(ex);
      }
    });
  } else {
    return func.apply(thisArg, functionArguments);
  }
}

function getFilenameWithoutExtension(path) {
  if (path == '' || path == '/') return null; // example: https://example.com/
  var pathParts = path.split('/');
  var filename = pathParts.pop();
  if (filename == '') { // example: https://example.com/directory/
    filename = pathParts.pop();
  }
  filename = filename.split('.');
  filename.pop();
  filename = filename.join('.');
  if (filename == '' || filename == '.') return null;
  return filename;
}

function createDownload(url, name, incognito) {
  if (typeof name != 'string') name = browser.i18n.getMessage('untitledFilename');

  // filename checks & filters
  var lowercaseName = name.toLowerCase();
  /* forbidden on windows */ if (lowercaseName == 'con' || lowercaseName == 'prn' || lowercaseName == 'aux' || lowercaseName == 'nul') name = browser.i18n.getMessage('untitledFilename');
  if (
    // forbidden on windows: /^(com|lpt)[0-9]$/i
    (lowercaseName.startsWith('com') || lowercaseName.startsWith('lpt'))
    && lowercaseName.length == 4
    && !isNaN(parseInt(lowercaseName.substr(3, 1)))
  ) name = browser.i18n.getMessage('untitledFilename');
  /* illegal characters */ name = name.replace(/[<>:"\\\/\|\?\*]/g, '_');

  var fileContent = {
    version: 1,
    url: url
  };
  fileContent = JSON.stringify(fileContent);
  var dlBlob = new Blob(
    [fileContent],
    {
      type: 'application/goto+json'
    }
  );
  var downloadOptions = {
    url: URL.createObjectURL(dlBlob),
    saveAs: true,
    filename: name + '.goto'
  }
  if (!isChrome) {
    // add the "incognito" option only if browser is not chrome because chrome doesn't support it and it throws an error if it's present
    downloadOptions.incognito = incognito;
  }
  p(this, browser.downloads.download, downloadOptions).then(() => {
    URL.revokeObjectURL(dlBlob);
  }).catch((err) => {
    console.error(err);
    URL.revokeObjectURL(dlBlob);
  });
}

browser.browserAction.onClicked.addListener(function(tab) {
  createDownload(tab.url, tab.title, tab.incognito);
});

// using the "contextMenus" api instead of "menus" to make chrome compatibility easier

browser.contextMenus.onClicked.addListener((info, tab) => {
  if (info.menuItemId == 'create') {
    // because the "command" option doesn't work in chrome
    createDownload(tab.url, tab.title, tab.incognito);
  } else if (info.menuItemId == 'createTab') {
    createDownload(tab.url, tab.title, tab.incognito);
  } else if (info.menuItemId == 'createFrame') {
    createDownload(info.frameUrl, getFilenameWithoutExtension(new URL(info.frameUrl).pathname), tab.incognito);
  } else if (info.menuItemId == 'createLink') {
    var text;
    try {
      // if the link text is a URL then the default filename should be the filename in the URL, without the extension.
      // example: https://example.com/image.png -> image
      var url = new URL(info.linkText);
      text = getFilenameWithoutExtension(url.pathname);
    } catch (ex) {
      // if the link text is NOT a URL then the default filename should be the link text.
      console.error(ex);
      text = info.linkText;
    }
    createDownload(info.linkUrl, text, tab.incognito);
  } else if (
    info.menuItemId == 'createImage'
    || info.menuItemId == 'createVideo'
    || info.menuItemId == 'createAudio'
  ) {
    createDownload(info.srcUrl, getFilenameWithoutExtension(info.srcUrl), tab.incognito);
  }
});

browser.contextMenus.create({
  id: 'create', // equivalent to the browser_action button
  //command: '_execute_browser_action', // doesn't work in chrome
  title: browser.i18n.getMessage('actionDefault'),
  contexts: [
    'audio',
    'frame',
    'image',
    'link',
    'page',
    'video'
  ]
});


if (!isChrome) {
  // chrome doesn't allow custom context menu actions in the tab menu
  browser.contextMenus.create({
    id: 'createTab', // create a file from a tab in the tab menu
    title: browser.i18n.getMessage('actionDefault'),
    contexts: [
      'tab'
    ]
  });
}

browser.contextMenus.create({
  id: 'createLink', // create a file from a link
  title: browser.i18n.getMessage('actionLink'),
  contexts: [
    'link'
  ]
});

browser.contextMenus.create({
  id: 'createFrame', // create a file from a frame
  title: browser.i18n.getMessage('actionFrame'),
  contexts: [
    'frame'
  ]
});

browser.contextMenus.create({
  id: 'createImage', // create a file from an img tag
  title: browser.i18n.getMessage('actionImage'),
  contexts: [
    'image'
  ]
});

browser.contextMenus.create({
  id: 'createVideo', // create a file from a video tag
  title: browser.i18n.getMessage('actionVideo'),
  contexts: [
    'video'
  ]
});

browser.contextMenus.create({
  id: 'createAudio', // create a file from an audio tag
  title: browser.i18n.getMessage('actionAudio'),
  contexts: [
    'audio'
  ]
});