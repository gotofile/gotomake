# GoTo maker

Create GoTo files directly from your browser.

## Build instructions

`node` and `npm` are required to build.

1. Install dependencies: `npm install`
2. Perform build: `npm run build`

The built zip file will be generated in `build/artifacts`.